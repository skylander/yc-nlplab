# all: nouns nouns-output
all: verbs nouns nouns-output

nouns: src/globals.foma src/nouns.foma
	@foma -f src/nouns.foma

nouns-output: src/globals.foma src/output.foma
	@foma -f src/output.foma

verbs: combined.lexc src/verbs.foma
	@foma -f src/verbs.foma

combined.lexc: src/verb-conjugations.lexc src/adj-conjugations.lexc lexicon/verbs-special_cases.lexc lexicon/verbs-stdforms.lexc lexicon/ignored.lexc  lexicon/v_special.lexc lexicon/v_kuru.lexc lexicon/v_suru.lexc lexicon/v_zuru.lexc lexicon/v1.lexc lexicon/v5_nu.lexc lexicon/v5_ku.lexc lexicon/v5_su.lexc lexicon/v5_mu.lexc lexicon/v5_ru.lexc lexicon/v5_tsu.lexc lexicon/v5_bu.lexc lexicon/v5_gu.lexc lexicon/v5_u.lexc lexicon/adj_i.lexc lexicon/adj_na.lexc
	@echo !!! Combined lexicon !!! > combined.lexc
	@echo LEXICON Root >> combined.lexc
	@echo >> combined.lexc
	@cd lexicon; cat adj_special.lexc adj_i.lexc adj_na.lexc >> ../combined.lexc
	@cd lexicon; cat v_kuru.lexc v_suru.lexc v_zuru.lexc v_special.lexc v5_u.lexc v5_bu.lexc v5_ku.lexc v5_nu.lexc v5_su.lexc v1.lexc v5_gu.lexc v5_mu.lexc v5_ru.lexc v5_tsu.lexc >> ../combined.lexc
	@cd lexicon; cat verbs-stdforms.lexc verbs-special_cases.lexc >> ../combined.lexc
	@cat src/verb-conjugations.lexc src/adj-conjugations.lexc >> combined.lexc
	@wc -l combined.lexc

clean:
	-@rm -fv nouns-output.fst nouns.fst combined.lexc verbs.fst

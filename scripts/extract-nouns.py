# -*- coding: utf-8 -*-
import argparse, sys, codecs, json, operator
import romkan

parser = argparse.ArgumentParser(description='Read JSON with token data and display nouns.')
A = parser.parse_args()

FST_NOUN = u'N'
TOKEN_POS_DELIMITER = u'/'
POSID_N_PREFIXES = set([30])
POSID_N_SUFFIXES = set([50, 51, 52, 53, 54, 55, 56, 57, 61, 62, 63, 64, 65, 66, 67])
POSID_N_VADVADJ = set([36, 37, 39, 40])
POSID_N_PROPER = set([38, 41, 42, 43, 44, 45, 46, 47, 48, 49])
POSID_N_PRO = set([59, 60])
POSID_N_DEPENDENT = POSID_N_PREFIXES | POSID_N_SUFFIXES

noun_phrases = {}

def unescape_unicode(o): return repr(o).decode('unicode-escape')

def add_noun_phrase(type, L, sent_id):
  global noun_phrases

  if not L: return False
  if len(L) == 1 and L[0][1] in POSID_N_DEPENDENT: return False# ignore those with one symbol of prefix only (usually these goes with verbs/adj etc)

  s = u'{}#{}$'.format(type, u' '.join(map(lambda (tok, pos): tok+ TOKEN_POS_DELIMITER +unicode(pos), L)))

  if s in noun_phrases: noun_phrases[s].add(sent_id)
  else: noun_phrases[s] = set([sent_id])
  
  return True
#end def

def process_sentence(sent, sent_id):
  fst_input = []

  for i, token in enumerate(sent):
    # if token['pos_id'] in [27, 28, 29]:
      # add_noun_phrase('N', [(token['token'], token['pos_id']), (sent[i+1]['token'], sent[i+1]['pos_id'])])
    #end if
    # continue

    if token['pos_id'] in POSID_N_PREFIXES: # noun prefixes
      fst_input.append((token['token'], token['pos_id']))
      # print "########",
      # add_noun_phrase('N', [(token['token'], token['pos_id']), (sent[i+1]['token'], sent[i+1]['pos_id'])])

    elif token['pos_id'] in POSID_N_VADVADJ: # in betweens (verbs, adverbs, adj, etc)
      fst_input.append((token['token'], token['pos_id']))

    elif token['pos_id'] in POSID_N_PROPER: # in-betweens (proper nouns)
      fst_input.append((token['token'], token['pos_id']))

    elif token['pos_id'] in POSID_N_PRO: # in-betweens (pronouns)
      fst_input.append((token['token'], token['pos_id']))

    elif token['pos_id'] in POSID_N_SUFFIXES: # suffixes
      fst_input.append((token['token'], token['pos_id']))

    elif fst_input:
      add_noun_phrase(FST_NOUN, fst_input, sent_id)
      fst_input = []
    #end if
  #end for

  add_noun_phrase(FST_NOUN, fst_input, sent_id)
#end def

if __name__ == "__main__":
  sys.stderr = codecs.getwriter('utf-8')(sys.stderr)
  sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
  sys.stdin = codecs.getreader('utf-8')(sys.stdin)

  cur_sent_id = None
  cur_sent = []
  for line in sys.stdin:
    line = line.strip()
    if not line: continue

    token_data = json.loads(line)


    if cur_sent_id == None or cur_sent_id == token_data['sent_id']:
      cur_sent.append(token_data)
    else:
      process_sentence(cur_sent, cur_sent_id)
      cur_sent = [token_data]
    #end if

    cur_sent_id = token_data['sent_id']
  #end for

  for s, sent_ids in noun_phrases.iteritems(): print s, u'# sent_ids=' + u','.join(map(unicode, sent_ids))
#end if

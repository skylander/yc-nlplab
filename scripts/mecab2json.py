# -*- coding: utf-8 -*-
import sys, codecs, operator, json
import romkan

def set_dict_value(d, k, v):
  if not v or v == '*': return

  d[k] = v
#end def

def process_sentence(sent_data, sent_id):
  context = map(operator.itemgetter(0), sent_data)

  for i, (token, token_type, features, pos_id) in enumerate(sent_data):
    context.insert(i, '{{')
    context.insert(i+2, '}}')
    token_info = {'token': token, 'pos_id': pos_id, 'context': u''.join(context), 'sent_id': sent_id, 'token_id': i + 1}
    del context[i]
    del context[i+1]

    if token_type == 1:
      pos, pos_sub1, pos_sub2, pos_sub3, vb_group, stem_form, dictionary_form = features.split(',')
      reading = None
      phonetic_reading = None
    else:
      pos, pos_sub1, pos_sub2, pos_sub3, vb_group, stem_form, dictionary_form, reading, phonetic_reading = features.split(',')
    #end if

    token_info['pos'] = pos
    set_dict_value(token_info, 'pos_sub1', pos_sub1)
    set_dict_value(token_info, 'pos_sub2', pos_sub2)
    set_dict_value(token_info, 'pos_sub3', pos_sub3)
    set_dict_value(token_info, 'vb_group', vb_group)
    set_dict_value(token_info, 'stem_form', stem_form)
    set_dict_value(token_info, 'dict_form', dictionary_form)
    set_dict_value(token_info, 'reading', reading)
    set_dict_value(token_info, 'phonetic', phonetic_reading)
    if phonetic_reading: set_dict_value(token_info, 'hepburn', romkan.to_hepburn(phonetic_reading))
    set_dict_value(token_info, 'unknown', True if token_type == 1 else False)
    # if pos_id in [10, 11, 12]: print token, features

    print json.dumps(token_info, ensure_ascii=False, sort_keys=True)
  #end for
#end def

if __name__ == "__main__":
  sys.stderr = codecs.getwriter('utf-8')(sys.stderr)
  sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
  sys.stdin = codecs.getreader('utf-8')(sys.stdin)

  sent_data = []
  sent_id = 0
  for lineno, line in enumerate(sys.stdin, start=1):
    line = line.strip('\n')
    if not line: continue

    token, token_type, features, pos_id = line.split(u'\t')

    if token == u'BOS':
      sent_data = []
      sent_id += 1
      continue
    #end if

    if token == u'EOS':
      process_sentence(sent_data, sent_id)
      continue
    #end if

    sent_data.append((token, int(token_type), features, int(pos_id)))
  #end for
#end if

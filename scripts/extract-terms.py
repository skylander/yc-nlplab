# -*- coding: utf-8 -*-
import argparse, sys, codecs, json, operator, re
import romkan

parser = argparse.ArgumentParser(description='Read JSON with token data and display verbs/adjectives.')
parser.add_argument('--details', default=False, action='store_true', help='Output details about every verb/adjective type extracted.')
parser.add_argument('--all', default=False, action='store_true', help='Output every verb/adjective token (regardless of type) extracted.')

group = parser.add_mutually_exclusive_group()
group.add_argument('--adjectives', default=False, action='store_true', help='Only care about adjectives.')
group.add_argument('--verbs', default=False, action='store_true', help='Only care about verbs.')
A = parser.parse_args()

FST_verb = u'V'
TOKEN_POS_DELIMITER = u'/'
POS_V = 31 #V
POS_V_SUFFIX = 32 #S
POS_V_DEP = 33 #D
POS_V_AUX = 25 #A
POS_V_PARTICLES = 18 #P

POS_ADJ_I = 10 #I
POS_ADJ_NA = 40 #N
# na adj - 形容動詞語幹
POS_ADJ_SUFFIX = 11 #T
POS_ADJ_DEP = 12 # seems like normal adj, but descriptive instead, treatinga s normal J

TOK_SUPPORTED_DEP = set([u'いる',u'下さる', u'でる'])
TOK_SUPPORTED_AUX = set([u'た', u'ない', u'ます', u'う', u'だ', u'ん', u'です', u'ぬ', u'まい', u'たい'])
TOK_SUPPORTED_PRT = set([u'て', u'ば', u'で'])
TOK_SUPPORTED_SFX = set([u'れる', u'られる', u'せる', u'させる'])

if A.verbs:
  RE_VP_ADJ = re.compile(ur'(V[ADPS]*)|([ds][SDAP]*)')

if A.adjectives:
  RE_VP_ADJ = re.compile(ur'([JN][APS]*)')

verb_phrases = {}

def unescape_unicode(o): return repr(o).decode('unicode-escape')

def add_verb_phrase(code, tokens, sent_id):
  global verb_phrases, A

  tok_list = map(operator.itemgetter('token'), tokens)
  s = u''.join(tok_list)

  if A.all:
    if A.details: print code + u'\t' + tokens[0].get('vb_group', '-') + u'\t' + tokens[0]['dict_form'] + u'\t' + u' '.join(tok_list) + u'\t' + s
    else: print s
  #end if

  if s in verb_phrases:
    verb_phrases[s].add(sent_id)
  else:
    verb_phrases[s] = set([sent_id])
    if A.details and not A.all:
      print code + u'\t' + tokens[0].get('vb_group', '-') + u'\t' + tokens[0]['dict_form'] + u'\t' + u' '.join(tok_list) + u'\t' + s
    # print code + u'\t' + tokens[0].get('vb_group', 'NA') + u'\t' + tokens[0]['dict_form'] + u'\t' + u' '.join(tok_list) + u'\t' + s
  #end if
#end def

def recognize_token(token):
  if token['pos_id'] == POS_V: return 'V' # can take all of these

  if token['pos_id'] == POS_V_PARTICLES:
    return 'P' if token['token'] in TOK_SUPPORTED_PRT else 'p'

  if token['pos_id'] == POS_V_AUX:
    return 'A' if token['dict_form'] in TOK_SUPPORTED_AUX else 'a'

  if token['pos_id'] == POS_V_SUFFIX:
    return 'S' if token['dict_form'] in TOK_SUPPORTED_SFX else 's'

  if token['pos_id'] == POS_V_DEP:
    return 'D' if token['dict_form'] in TOK_SUPPORTED_DEP else 'd'

  if token['pos_id'] == POS_ADJ_I: return 'J' # can take all of these
  if token['pos_id'] == POS_ADJ_NA: return 'N' # can take all of these

  if token['pos_id'] == POS_ADJ_DEP: return 'J'

  if token['pos_id'] == POS_ADJ_SUFFIX: return 'J'

  return '-'
#end def

def process_sentence(sent, sent_id):
  sent_mapped = u''.join(map(recognize_token, sent))

  for m in RE_VP_ADJ.finditer(sent_mapped):
    start, end = m.span()
    N = len(m.group())

    add_verb_phrase(m.group(), sent[start:end], sent_id)
  #end for
#end def

if __name__ == "__main__":
  sys.stderr = codecs.getwriter('utf-8')(sys.stderr)
  sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
  sys.stdin = codecs.getreader('utf-8')(sys.stdin)

  cur_sent_id = None
  cur_sent = []
  for line in sys.stdin:
    line = line.strip()
    if not line: continue

    token_data = json.loads(line)

    if cur_sent_id == None or cur_sent_id == token_data['sent_id']:
      cur_sent.append(token_data)
    else:
      process_sentence(cur_sent, cur_sent_id)
      cur_sent = [token_data]
    #end if

    cur_sent_id = token_data['sent_id']
  #end for

  if not A.details and not A.all:
    for s, sent_ids in verb_phrases.iteritems():
      print s # + u'\t# sent_ids=' + u','.join(map(unicode, sent_ids))
#end if

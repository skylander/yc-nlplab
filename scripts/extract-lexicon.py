# -*- coding: utf-8 -*-
import argparse, sys, codecs, json, operator, re, collections
from lxml import etree
import gzip

parser = argparse.ArgumentParser(description='Extract verb types from given corpus.')
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--tanaka-corpus', default=False, action='store_true', help='Input is Tanaka corpus JSON data.')
group.add_argument('--jmdict-corpus', default=False, action='store_true', help='Input is GZipped JMDict data.')
A = parser.parse_args()

"""
     18 ラ変
  38499 サ変・スル
   2681 カ変・クル
    369 サ変・−スル
     34 サ変・−ズル
   1989 カ変・来ル
      3 四段・ハ行
      2 四段・バ行
   1156 五段・ガ行
  11057 五段・サ行
   4169 五段・タ行
  43613 五段・ラ行
    573 五段・ナ行
   2345 五段・バ行
   6216 五段・マ行
   5359 五段・カ行促音便
  21317 五段・ワ行促音便
  12108 五段・カ行イ音便
     41 五段・カ行促音便ユク
      3 五段・ワ行ウ音便
   5898 五段・ラ行特殊
  87450 一段
   2603 一段・クレル
     27 一段・得ル
      5 上二・ダ行
      2 下二・ガ行
      5 下二・カ行
      1 下二・ダ行
      1 下二・マ行
"""

POS_V = set([31, 32, 33])
POS_I_ADJ = set([10, 11, 12])
POS_NA_ADJ = set([40])
POS_ADJ = POS_I_ADJ | POS_NA_ADJ

VB_GROUP_V5_REGULAR = set(['v5_bu', 'v5_gu', 'v5_ku', 'v5_mu', 'v5_nu', 'v5_ru', 'v5_su', 'v5_tsu', 'v5_u'])

verb_groups = collections.defaultdict(set)

RE_suru = re.compile(ur'する$', flags=re.U)
RE_zuru = re.compile(ur'ずる$', flags=re.U)
RE_kuru = re.compile(ur'[来く]る$', flags=re.U)
RE_v5 = re.compile(ur'[ぐすつるぬぶむくう]$', flags=re.U)
RE_v1 = re.compile(ur'る$', flags=re.U)
RE_adj_i = re.compile(ur'い$', flags=re.U)

def add_verb_tanaka(token, sent_id):
  global verb_groups

  # s = u'{}#{}$'.format(type, u' '.join(map(lambda (tok, pos): tok + TOKEN_POS_DELIMITER + unicode(pos), L)))
  s = ''
  dict_form = token['dict_form']
  vb_group = None

  if token['vb_group'] in [u'サ変・スル', u'サ変・−スル']:
    stem = RE_suru.sub(u'', dict_form)
    vb_group = 'v_suru'
  elif token['vb_group'] in [u'カ変・クル', u'カ変・来ル']:
    stem = RE_kuru.sub(u'', dict_form)
    vb_group = 'v_kuru'
  elif token['vb_group'] == u'サ変・−ズル':
    stem = RE_zuru.sub(u'', dict_form)
    vb_group = 'v_zuru'
  elif token['vb_group'] == u'五段・ガ行':
    stem = RE_v5.sub(u'', dict_form)
    vb_group = 'v5_gu'
  elif token['vb_group'] == u'五段・サ行':
    stem = RE_v5.sub(u'', dict_form)
    vb_group = 'v5_su'
  elif token['vb_group'] == u'五段・タ行':
    stem = RE_v5.sub(u'', dict_form)
    vb_group = 'v5_tsu'
  elif token['vb_group'] == u'五段・ラ行':
    stem = RE_v5.sub(u'', dict_form)
    vb_group = 'v5_ru'
  elif token['vb_group'] == u'五段・ナ行':
    stem = RE_v5.sub(u'', dict_form)
    vb_group = 'v5_nu'
  elif token['vb_group'] == u'五段・バ行':
    stem = RE_v5.sub(u'', dict_form)
    vb_group = 'v5_bu'
  elif token['vb_group'] == u'五段・マ行':
    stem = RE_v5.sub(u'', dict_form)
    vb_group = 'v5_mu'
  elif token['vb_group'] in [u'五段・カ行促音便', u'五段・カ行イ音便', u'五段・カ行促音便ユク']:
    stem = RE_v5.sub(u'', dict_form)
    vb_group = 'v5_ku'
  elif token['vb_group'] in [u'五段・ワ行促音便', u'五段・ワ行ウ音便']:
    stem = RE_v5.sub(u'', dict_form)
    vb_group = 'v5_u'
  elif token['vb_group'] in [u'一段', u'一段・クレル']:
    stem = RE_v1.sub(u'', dict_form)
    vb_group = 'v1'

  elif token['dict_form'] in [u'なさる', u'ござる', u'いらっしゃる', ]:
    stem = dict_form
    vb_group = 'v_special'

  else:
    stem = dict_form
    vb_group = 'ignored'
  #end if

  verb_groups[vb_group].add(stem)

  return True
#end def

def add_adj_tanaka(token, sent_id):
  global verb_groups

  # s = u'{}#{}$'.format(type, u' '.join(map(lambda (tok, pos): tok + TOKEN_POS_DELIMITER + unicode(pos), L)))
  s = ''
  dict_form = token['dict_form']
  vb_group = 'ignored'
  stem = dict_form
  if token['pos_id'] in POS_NA_ADJ:
    vb_group = 'adj_na'
    stem = dict_form
  elif token['pos_id'] in POS_I_ADJ:
    if dict_form in [u'よい']:
      stem = dict_form
      vb_group = 'adj_special'
    else:
      stem = RE_adj_i.sub(u'', dict_form)
      vb_group = 'adj_i'
    #end if
  #end if

  verb_groups[vb_group].add(stem)

  return True
#end def

def add_jmdict_entry(el_entry):
  pos_set = set()
  for el_sense in el_entry.iterfind('sense'):
    for el_pos in el_sense.iterfind('pos'):
      if el_pos is None or el_pos.text in pos_set: continue

      if el_pos.text == 'Ichidan verb': pos_set.add('v1')
      elif el_pos.text == 'Yodan verb with `hu/fu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Yodan verb with `ru\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Godan verb (not completely classified)': pos_set.add('ignored')
      elif el_pos.text == 'Godan verb - -aru special class': pos_set.add('ignored')
      elif el_pos.text == 'Godan verb with `bu\' ending': pos_set.add('v5_bu')
      elif el_pos.text == 'Godan verb with `gu\' ending': pos_set.add('v5_gu')
      elif el_pos.text == 'Godan verb with `ku\' ending': pos_set.add('v5_ku')
      elif el_pos.text == 'Godan verb - Iku/Yuku special class': pos_set.add('ignored')
      elif el_pos.text == 'Godan verb with `mu\' ending': pos_set.add('v5_mu')
      elif el_pos.text == 'Godan verb with `nu\' ending': pos_set.add('v5_nu')
      elif el_pos.text == 'Godan verb with `ru\' ending': pos_set.add('v5_ru')
      elif el_pos.text == 'Godan verb with `ru\' ending (irregular verb)': pos_set.add('ignored')
      elif el_pos.text == 'Godan verb with `su\' ending': pos_set.add('v5_su')
      elif el_pos.text == 'Godan verb with `tsu\' ending': pos_set.add('v5_tsu')
      elif el_pos.text == 'Godan verb with `u\' ending': pos_set.add('v5_u')
      elif el_pos.text == 'Godan verb with `u\' ending (special class)': pos_set.add('ignored')
      elif el_pos.text == 'Godan verb - Uru old class verb (old form of Eru)': pos_set.add('ignored')
      elif el_pos.text == 'Ichidan verb - zuru verb (alternative form of -jiru verbs)': pos_set.add('v_zuru')
      elif el_pos.text == 'Kuru verb - special class': pos_set.add('v_kuru')
      elif el_pos.text == 'irregular nu verb': pos_set.add('ignored')
      elif el_pos.text == 'irregular ru verb, plain form ends with -ri': pos_set.add('ignored')
      elif el_pos.text == 'noun or participle which takes the aux. verb suru': pos_set.add('v_suru')
      elif el_pos.text == 'su verb - precursor to the modern suru': pos_set.add('ignored')
      elif el_pos.text == 'suru verb - special class': pos_set.add('ignored')
      elif el_pos.text == 'suru verb - irregular': pos_set.add('ignored')
      elif el_pos.text == 'Yodan verb with `ku\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Yodan verb with `gu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Yodan verb with `su\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Yodan verb with `tsu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Yodan verb with `nu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Yodan verb with `bu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Yodan verb with `mu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (upper class) with `hu/fu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (upper class) with `ku\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (upper class) with `gu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (upper class) with `tsu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (upper class) with `dzu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (upper class) with `bu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (upper class) with `mu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (upper class) with `yu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (upper class) with `ru\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (lower class) with `ku\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (lower class) with `gu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (lower class) with `su\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (lower class) with `zu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (lower class) with `tsu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (lower class) with `dzu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (lower class) with `nu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (lower class) with `hu/fu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (lower class) with `bu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (lower class) with `mu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (lower class) with `yu\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (lower class) with `ru\' ending (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'Nidan verb (lower class) with `u\' ending and `we\' conjugation (archaic)': pos_set.add('ignored')

      elif el_pos.text == 'adjective (keiyoushi)': pos_set.add('adj_i')
      elif el_pos.text == 'adjectival nouns or quasi-adjectives (keiyodoshi)': pos_set.add('adj_na')
      elif el_pos.text == '`kari\' adjective (archaic)': pos_set.add('ignored')
      elif el_pos.text == '`ku\' adjective (archaic)': pos_set.add('ignored')
      elif el_pos.text == '`shiku\' adjective (archaic)': pos_set.add('ignored')
      elif el_pos.text == 'archaic/formal form of na-adjective': pos_set.add('ignored')
      elif el_pos.text == 'nouns which may take the genitive case particle `no\'': pos_set.add('ignored')
      elif el_pos.text == 'pre-noun adjectival (rentaishi)': pos_set.add('ignored')
      elif el_pos.text == '`taru\' adjective': pos_set.add('ignored')
    #end for
  #end for

  if not pos_set: return

  readings = set()
  for el_kele in el_entry.iterfind('k_ele'):
    for el_keb in el_kele.iterfind('keb'):
      keb = el_keb.text
      readings.add(keb)
    #end for
  #end for

  for el_rele in el_entry.iterfind('r_ele'):
    for el_reb in el_rele.iterfind('reb'):
      reb = el_reb.text
      readings.add(reb)
    #end for
  #end for

  for vb_group in pos_set:
    if vb_group in VB_GROUP_V5_REGULAR: verb_groups[vb_group].update(map(lambda r: RE_v5.sub(u'', r), readings))
    elif vb_group == 'v_zuru': verb_groups[vb_group].update(map(lambda r: RE_zuru.sub(u'', r), readings))
    elif vb_group == 'v_kuru': verb_groups[vb_group].update(map(lambda r: RE_kuru.sub(u'', r), readings))
    elif vb_group == 'v_suru': verb_groups[vb_group].update(map(lambda r: RE_suru.sub(u'', r), readings))
    elif vb_group == 'v1': verb_groups[vb_group].update(map(lambda r: RE_v1.sub(u'', r), readings))

    elif vb_group == 'adj_i': verb_groups[vb_group].update(map(lambda r: RE_adj_i.sub(u'', r), readings))

    else: verb_groups[vb_group].update(readings)
  #end for
#end def

if __name__ == "__main__":
  sys.stderr = codecs.getwriter('utf-8')(sys.stderr)
  sys.stdout = codecs.getwriter('utf-8')(sys.stdout)

  if A.tanaka_corpus:
    sys.stdin = codecs.getreader('utf-8')(sys.stdin)

    cur_sent_id = None
    cur_sent = []
    for line in sys.stdin:
      line = line.strip()
      if not line: continue

      token_data = json.loads(line)

      if cur_sent_id == None or cur_sent_id == token_data['sent_id']:
        cur_sent.append(token_data)
      else:
        for i, token in enumerate(cur_sent):
          if token['pos_id'] in POS_V:
            add_verb_tanaka(token, cur_sent_id)
          elif token['pos_id'] in POS_ADJ:
            add_adj_tanaka(token, cur_sent_id)
          #end if
        #end for

        cur_sent = [token_data]
      #end if

      cur_sent_id = token_data['sent_id']
    #end for
  elif A.jmdict_corpus:
    with gzip.GzipFile(mode='rb', fileobj=sys.stdin) as f:
      f = codecs.getreader('utf-8')(f)
      parser = etree.XMLParser(remove_blank_text=True)
      tree = etree.parse(f, parser)
      root = tree.getroot()
      print >>sys.stderr, 'Loaded corpus, finding verbs...'

      for el_entry in root.iterfind('entry'):
        add_jmdict_entry(el_entry)
    #end with
  #end if

  count = 0
  for vb_group, stems in verb_groups.iteritems():
    for stem in stems:
      print json.dumps({'vb_group': vb_group, 'stem': stem}, ensure_ascii=False)
      count += 1
  #end for
  print >>sys.stderr, 'Found {} lexical items.'.format(count)
#end if

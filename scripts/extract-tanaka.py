import sys, codecs, re

# Extract Japanese sentences from Tanaka corpus.

re_tanaka_sent = re.compile(r'^A\: (.+)\t(.+)#ID=([\d_]+)$', re.U)

sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
sys.stdin = codecs.getreader('utf-8')(sys.stdin)

for i, line in enumerate(sys.stdin, start=1):
  if not line.startswith('A:'): continue
  m = re_tanaka_sent.match(line.strip())
  
  jp_text = m.group(1)
  print jp_text
#end for

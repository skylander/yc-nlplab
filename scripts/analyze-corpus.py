# -*- coding: utf-8 -*-
import argparse, sys, codecs, json, operator, collections, re

parser = argparse.ArgumentParser(description='Read JSON with token data and display nouns.')
A = parser.parse_args()

FST_NOUN = u'N'
POSID_N_PREFIXES = set([30])
POSID_N_SUFFIXES = set([50, 51, 52, 53, 54, 55, 56, 57, 61, 62, 63, 64, 65, 66, 67])
POSID_N_VADVADJ = set([36, 37, 39, 40])
POSID_N_PROPER = set([38, 41, 42, 43, 44, 45, 46, 47, 48, 49])
POSID_N_PRO = set([59, 60])
POSID_N_DEPENDENT = POSID_N_PREFIXES | POSID_N_SUFFIXES

POS_V = set([31, 32, 33])
POS_I_ADJ = set([10, 11, 12])
POS_NA_ADJ = set([40])

POS_ID_TRANS = {31: 'V', 32: 'S', 33: 'D', 25: 'A', 18: 'A'}
RE_VP = re.compile(r'V[SDA]*')

def unescape_unicode(o): return repr(o).decode('unicode-escape')

suffix = collections.Counter()
def process_sentence(sent, sent_id):
  global suffix
  
  # sent_mapped = u''.join(map(lambda token: POS_ID_TRANS.get(token['pos_id'], '-'), sent))
  # for m in RE_VP.finditer(sent_mapped):
    # start, end = m.span()

  for i, token in enumerate(sent):
    if token['pos_id'] in POS_NA_ADJ:
      suffix[token['dict_form']] += 1
      print token['dict_form']
    #end for
  #end for
  
  # for i, token in enumerate(sent):
   # if token['pos_id'] in [18]:
      # print unescape_unicode(token['dict_form'])
      # suffix[token['token']] += 1
      # print 
    # if i + 1 < len(sent) and token['pos_id'] in [32] and sent[i+1]['pos_id'] in [31]:
      # print unescape_unicode(token)
      # print unescape_unicode(sent[i+1])
      # print 
#end def

if __name__ == "__main__":
  sys.stderr = codecs.getwriter('utf-8')(sys.stderr)
  sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
  sys.stdin = codecs.getreader('utf-8')(sys.stdin)

  # print unescape_unicode(set([u'\u30c1\u30e3\u30f3', u'\u3055\u307e', u'\u30af\u30f3', u'\u3055\u3093', u'\u3061\u3083\u3093', u'\u3061\u3093', u'\u30b5\u30de', u'\u6c0f', u'\u8457', u'\u3069\u306e', u'\u69d8', u'\u541b', u'\u304f\u3093']))

  cur_sent_id = None
  cur_sent = []
  for line in sys.stdin:
    line = line.strip()
    if not line: continue

    token_data = json.loads(line)


    if cur_sent_id == None or cur_sent_id == token_data['sent_id']:
      cur_sent.append(token_data)
    else:
      process_sentence(cur_sent, cur_sent_id)
      cur_sent = [token_data]
    #end if

    cur_sent_id = token_data['sent_id']
  #end for

  # print unescape_unicode(suffix.most_common())
  print >>sys.stderr, len(suffix)
#end if

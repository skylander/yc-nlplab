# -*- coding: utf-8 -*-
import argparse, sys, codecs, json, operator, re, collections
import romkan

parser = argparse.ArgumentParser(description='Build FOMA lexicon of Japanese lexical items from verb lexicon JSON files.')
parser.add_argument('json_files', type=file, nargs='+', help='Set of JSON formatted lexical items to include in FOMA lexicon.')
A = parser.parse_args()

verb_groups = collections.defaultdict(set)
LEXC_FILENAME = 'lexicon/{}.lexc'

def unescape_unicode(o): return repr(o).decode('unicode-escape')

if __name__ == "__main__":
  sys.stderr = codecs.getwriter('utf-8')(sys.stderr)
  sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
  sys.stdin = codecs.getreader('utf-8')(sys.stdin)

  for f in A.json_files:
    f = codecs.getreader('utf-8')(f)

    count = 0
    for line in f:
      vb_data = json.loads(line)
      verb_groups[vb_data['vb_group']].add(vb_data['stem'])
      count += 1
    #end for

    print >>sys.stderr, 'Read {} entries from {}'.format(count, f.name)
  #end for

  count = 0
  for vb_group, stems in verb_groups.iteritems():
    with codecs.open(LEXC_FILENAME.format(vb_group), 'w', 'utf-8') as f:
      print >> f, u'!!! verb lexicon for {} !!!'.format(vb_group)
      print >> f, u''

      if vb_group == 'v_special':
        print >> f, u'v_special;'
        continue
      #end if
      if vb_group == 'adj_special':
        print >> f, u'adj_special;'
        continue
      #end if

      for stem in stems:
        if stem:
          print >> f, u'{} {};'.format(stem, vb_group)
        else:
          print >> f, u'{};'.format(vb_group)
      #end for

      print >> f, u''
      print >>sys.stderr, 'Written {} lexical items to {}'.format(len(stems), f.name)
      count += len(stems)
    #end with
  #end for

  print >>sys.stderr, ''
  print >>sys.stderr, 'Written {} lexical items in total.'.format(count)
#end if

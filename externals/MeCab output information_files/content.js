/** コンテンツ出力機能 */
var Mwsoft = { Content: {} };

/** ヘッダの出力 */
Mwsoft.Content.header = function(title, opt) {
  document.write('\
    <div id="header">\
      <h1>' + title + '</h1>\
    </div>\
    <div id="container">\
  ');
  /*
    <div style="position:absolute; top: 40px; right:10px;">\
      <a class="menu" href="/">\
        <img style="width: 30px; height: 30px;" src="/site/img/logo.png" alt="logo">\
      </a>\
    </div>\
  */
  if( opt.sidebarType == "menu" )
    Mwsoft.Content.menuSidebar( opt );
  else if( opt.sidebarType == "short" )
    Mwsoft.Content.shortSidebar( opt );
  else
    Mwsoft.Content.articleSidebar( opt );
  document.write('<div id="content">');
};

/** フッタの出力 */
Mwsoft.Content.footer = function(cc) {
  var footer = '</div></div>\
      <div id="footer">'
  if(this.cc) footer += '<div class="ccommons">\
        <a rel="license" href="http://creativecommons.org/licenses/by/3.0/">\
          <img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by/3.0/88x31.png" />\
        </a>\
      </div>'
  footer += '<div class="copyright" style="padding-top:8px">Copyright &copy; 2012 <a href="http://www.mwsoft.jp/">mwSoft</a></div>\
      </div>'
  document.write(footer);
};

/** サイドバーの出力 */
Mwsoft.Content.sidebar = function(contents, opt) {
  var opt = (!opt) ? {} : opt;
  var sidebar = document.write('<div id="leftnavi">');
  for( key in contents )
    contents[key]( opt );
  document.write('</div>');
};

/** メニュー用サイドバー出力 */
Mwsoft.Content.menuSidebar = function( opt ) {
  opt = !opt ? {} : opt;
  if(opt.entryCount == undefined) opt.entryCount = 5;
  this.sidebar( [
    this.pankuzu,
    this.siteSearch,
    this.profile,
    this.freeSpace,
    this.recentEntry
  ], opt );
};

/** 記事用サイドバー出力 */
Mwsoft.Content.articleSidebar = function( opt ) {
  opt = !opt ? {} : opt;
  if(opt.entryCount == undefined) opt.entryCount = 10;
  this.sidebar( [
    this.pankuzu,
    this.siteSearch,
    this.profile,
    this.freeSpace,
    this.similarPages,
    this.recentEntry
  ], opt );
};

/** 最低限の構成のサイドバー出力 */
Mwsoft.Content.shortSidebar = function( opt ) {
  opt = !opt ? {} : opt;
  this.sidebar( [
    this.pankuzu,
    this.profile,
    this.freeSpace,
  ], opt );
};

Mwsoft.Content.freeSpace = function() {
}

/** サイト検索の出力 */
Mwsoft.Content.siteSearch = function() {
  document.write('\
    <div class="menu">\
      <h3>Site Search</h3>\
      <form method=get action="http://www.google.co.jp/search">\
      <input type="text" name="q" maxlength="255" value="" style="width:148px"><br>\
      <input type="hidden" name="ie" value="UTF-8">\
      <input type="hidden" name="oe" value="UTF-8">\
      <input type="hidden" name="hl" value="ja">\
      <input type="hidden" name="domains" value="mwsoft.jp">\
      <input type="hidden" name="sitesearch" value="mwsoft.jp">\
      <input class="search_button" type="submit" name="btnG" value="検索">\
      </form>\
    </div>'
  );
};

/** パンくず用の情報 */
Mwsoft.Content.pankuzuSettings = {
  '/': 'Top Page',
  '/programming/': 'Programming',
  '/programming/munou/': 'NLP',
  '/programming/nlp/': 'NLP',
  '/programming/play/': 'Play',
  '/programming/scala/': 'Scala',
  '/programming/lucene/': 'Lucene/Solr',
  '/programming/javascript/': 'Javascript',
  '/programming/java/': 'Java',
  '/programming/hadoop/': 'Hadoop',
  '/programming/r/': 'R',
  '/programming/mahout/': 'Mahout',
  '/programming/keita/': '形態素解析',
  '/column/': 'コラム',
  '/column/novel/': 'プログラマ文庫'
};

/** パンくずの出力 */
Mwsoft.Content.pankuzu = function( opt ) {
  var pankuzuList = !opt.pankuzu ? [] : opt.pankuzu;
  var len = pankuzuList.length;
  if( len > 0 ) {
    var html = '<div class="menu"><h3>Menu</h3>';
    for( var i = 0; i < len; i++ ) {
      var key = pankuzuList[i];
      var value = Mwsoft.Content.pankuzuSettings[key];
      if(value)
        html += '<p style="margin: 5px 0px 5px 10px"><a href=' + key + ' class="menu">' + Mwsoft.Content.pankuzuSettings[key] + '</a>'
    }
    html += '</div>';
    document.write(html);
  }
};

/** プロフィールの出力 */
Mwsoft.Content.profile = function( opt ) {
  document.write('\
    <div class="menu">\
      <h3>Profile</h3>\
      <dl>\
      <dt class="profile">Name</dt><dd class="profile">Masato Watanabe</dd>\
      <dt class="profile">Twitter</dt><dd class="profile"><a class="menu" href="http://twitter.com/#!/mwsoft_jp">@mwsoft_jp</a></dd>\
      <dt class="profile">Contact</dt><dd class="profile"><a class="menu" href="https://mwsoft.sakura.ne.jp/request/blog_request_form.html">フォームよりどうぞ</a></dd>\
      </dl>\
    </div>');
};

/** 最近のエントリーの出力 */
Mwsoft.Content.recentEntry = function( opt ) {
  if((!opt || opt.entryCount > 0) && location.host != "localhost") {
    Mwsoft.Content.opt = !opt ? {} : opt;
    document.write('<div id="recent_entry"></div>')
    $.getJSON("http://b.hatena.ne.jp/entrylist/json?url=http%3A%2F%2Fwww.mwsoft.jp%2F&callback=?",
      function(data){
        var entryCount = Mwsoft.Content.opt.entryCount;
        var entryCount = (!entryCount) ? 10 : entryCount;
        var len = data.length;
        var str = '<h3>最近のエントリー</h3>';
        for( var i = 0; i < len; i++ ) {
          if(entryCount <= i) break;
          str += '<p><a class="menu" href="' + data[i]['link'] + '">' + data[i]['title'] +'</a>&nbsp;&nbsp;\
              <a class="entry_count" href="http://b.hatena.ne.jp/entry/' + escape(data[i]['link']) + '">' + data[i]['count'] + 'user</a>';
        }
        $("#recent_entry").addClass( "menu" );
        $("#recent_entry").html( str );
      }
    );
  }
};

/** シンプルな棒グラフを生成する */
Mwsoft.Content.writeSimpleBarGraph = function( id, data, xlabel, ylabel, ymin, ymax ) {
  if( $.jqplot ) {
    $.jqplot(id, [data], {
      series:[{renderer:$.jqplot.BarRenderer}],
      axes: {
        xaxis: {
          renderer: $.jqplot.CategoryAxisRenderer,
          label: xlabel
        },
        yaxis: {
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
          label: ylabel,
          min: 0,
          max: ymax
        }
      }
    });
  }
}

Mwsoft.Content.similarPages = function( opt ) {
  document.write('<div id="similar_entry"></div>');
  var pathname = location.pathname;
  //var dir = pathname.substring(0, pathname.lastIndexOf("/"));
  var filename = pathname.substring(pathname.lastIndexOf("/") + 1);
  $.get( "./js/" + filename + ".json", function(data) {
    var str = "<h2>関連エントリー</h2>";
    var data = eval(data);
    if(data.length > 0) {
      var len = data.length < 5 ? data.length : 5;
      for( var i = 0; i < len; i++ )
        str += '<p><a class="menu" href="' + data[i]['link'] + '">' + data[i]['title'] + '</a>'
      $("#similar_entry").html( str );
      $("#similar_entry").addClass( "menu" );
    }
    else {
      alert("else");
      $("#similar_entry").html( str + "<p>関連エントリーは見つかりませんでした" );
    }
  } );
}

Mwsoft.Content.simpleAmazonAd = function( id ) {
  return '<div class="menu">\
    <h2>広告</h2>\
    <p><a href="http://www.amazon.co.jp/gp/product/' + id + '/ref=as_li_tf_il?ie=UTF8&tag=mwsoft-22&linkCode=as2&camp=247&creative=1211&creativeASIN=' + id + '"><img border="0" src="http://ws.assoc-amazon.jp/widgets/q?_encoding=UTF8&Format=_SL160_&ASIN=' + id + '&MarketPlace=JP&ID=AsinImage&WS=1&tag=mwsoft-22&ServiceVersion=20070822" ></a><img src="http://www.assoc-amazon.jp/e/ir?t=mwsoft-22&l=as2&o=9&a=' + id + '" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" /></div>';
}


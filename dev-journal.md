# Development journal

## Preparing the data

This section is concerned with taking the raw Tanaka corpus and get it into a format suitable for MeCab, and then transforming that MeCab format into a Python/myself friendly format for analyzing.

### Dependencies

  * [Tanaka Corpus](http://www.edrdg.org/wiki/index.php/Tanaka_Corpus)
  * [MeCab](http://mecab.googlecode.com/)
  * [python-romkan](http://www.soimort.org/python-romkan/)

### Extracting relevant sentences from the Tanaka corpus

`extract-tanaka.py` reads in the Tanaka corpus from `STDIN` and extracts the Japanese sentences (without the English translation or `B` parts) and outputs it to `STDOUT` line by line.

    zcat corpora/tanaka-corpus.utf8 | pv -l --size 299818 | python extract-tanaka.py > data/tanaka-corpus.utf8.sents

### Pass the sentences through MeCab

We take these sentences and run it through MeCab in the `nlplab` format (specified below).

    pv -l data/tanaka-corpus.utf8.sents | mecab -Onlplab > data/tanaka-corpus.utf8.mecab

The MeCab `nlplab` output format:

    ; my own format
    ; 0: normal, 1: unknown, 2: bos, 3: eos
    node-format-nlplab = %m\\t%s\\t%H\\t%h\\n
    unk-format-nlplab = %m\\t%s\\t%H\\t%h\\n
    eos-format-nlplab = EOS\\t%s\\t%H\\t%h\\n
    bos-format-nlplab = BOS\\t%s\\t%H\\t%h\\n
    unk-feature-nlplab = UNK\\n

### Convert MeCab output to JSON format

Now, `mecab2json.py` takes the MeCab format (from `STDIN`) and turns it into a structured JSON file at a token level, one JSON object per token (for each line) (to `STDOUT`).

    pv -l data/tanaka-corpus.utf8.mecab | python mecab2json.py > data/tanaka-corpus.utf8.json
    zcat data/tanaka-corpus.utf8.mecab.gz | pv -l --size 2060544 | python mecab2json.py > data/tanaka-corpus.utf8.json

JSON format looks like

    {"context": "ムーリエルは２０歳{{に}}なりました。", "dict_form": "に", "hepburn": "ni", "phonetic": "ニ", "pos": "助詞", "pos_id": 13, "pos_sub1": "格助詞", "pos_sub2": "一般", "reading": "ニ", "sent_id": 1, "token": "に", "token_id": 6}

Each line of JSON refers to a token.

  * `context` - is the sentence containing the token,
  * `token` refers to the actual token string and `unknown` refers to whether MeCab knows what this token is about.
  * `sent_id` and `token_id` - unique sentence and token id,
  * `pos` - the POS tag of the token,
  * `pos_id` - the POS tag of the token as defined by MeCab (<http://mecab.googlecode.com/svn/trunk/mecab/doc/posid.html>),
  * `pos_subN` - the sub POS tags,
  * `stem_form` - stem form of the token (if exists),
  * `dict_form` - dictionary form of the token (if exists),
  * `vb_group` - the verb group of the token (if exists),
  * `reading` - reading of the token,
  * `phonetic` - surface form reading of the token (which maybe different from how the word itself is read (i.e は the particle), includes the long vowel marker 'ー',
  * `hepburn` - Hepburn romaji encoding of the `phonetic` reading (based on the `python-romkan` library),
  * `unknown` - if MeCab does not know what it is, in this case it will usually be tagged as some kind of noun.

### Doing everything together

    zcat corpora/tanaka-corpus.utf.gz | pv -l --size 299818 | python extract-tanaka.py | mecab -Onlplab | python mecab2json.py | gzip > data/tanaka-corpus.utf8.json.gz

## Preparing nouns

### MeCab noun classification

This is a list of approximate Japanese -> English translation of MeCab POS categories for nouns.

  * 接頭詞,名詞接続,*,* 30 - Noun prefixes
  * 名詞,サ変接続,*,* 36 - Suru verbs that are "noun-ified", kinda like a gerund.
  * 名詞,ナイ形容詞語幹,*,* 37 - Nai forms that are "noun-ified", nominal adjectives, adjectival nouns.
  * 名詞,一般,*,* 38 - Normal nouns.
  * 名詞,引用文字列,*,* 39 - Quoted nouns (only one instance in corpus).
  * 名詞,形容動詞語幹,*,* 40 - I forms that are "noun-ified", adjectival nouns.
  * 名詞,固有名詞,一般,* 41 - Proper nouns, normal (and which do not apply to the next few).
  * 名詞,固有名詞,人名,一般 42 - Proper nouns, person names, normal.
  * 名詞,固有名詞,人名,姓 43 - Proper nouns, person names, last names.
  * 名詞,固有名詞,人名,名 44 - Proper nouns, person names, first names.
  * 名詞,固有名詞,組織,* 45 - Proper nouns, organizations.
  * 名詞,固有名詞,地域,一般 46 - Proper nouns, locations, normal.
  * 名詞,固有名詞,地域,国 47 - Proper nouns, locations, nations.
  * 名詞,数,*,* 48 - Cardinal numbers (arabic, kanji, kana).
  * 名詞,接続詞的,*,* 49 - Conjunctive nouns? seems more like conjunctions to me.
  * 名詞,接尾,サ変接続,* 50 - Suffixes, irregular, conjunctives.
  * 名詞,接尾,一般,* 51 - Suffixes, normal, (-tachi is one of them)
  * 名詞,接尾,形容動詞語幹,* 52 - Suffixes, used with adjectival verbs, i.e 「がち」、「的」、「同然」
  * 名詞,接尾,助数詞,* 53 - Suffixes for measure words, numeral classifiers.
  * 名詞,接尾,助動詞語幹,* 54 - Suffixes used with aux verbs? 「そ」、「そう」のみ?
  * 名詞,接尾,人名,* 55 - Suffixes used with proper nouns/people names.
  * 名詞,接尾,地域,* 56 - Suffixes used with locations, i.e 「州」、「市内」、「港」.
  * 名詞,接尾,特殊,* 57 - Suffixes, special? 「ぶり」、「み」、「方」? Seems to be common with some adjectives.
  * 名詞,接尾,副詞可能,* 58 - Possible adverbial noun? Seems to be describing the process, like end of (末), midst of (中), after (後), etc.
  * 名詞,代名詞,一般,* 59 - Pronouns.
  * 名詞,代名詞,縮約,* 60 - Pronouns contractions (mostly in spoken, none found in Tanaka corpus).
  * 名詞,動詞非自立的,*,* 61 - -goran and -chodai's. Which seem to be more like verbal phrase suffix to me though.
  * 名詞,特殊,助動詞語幹,* 62 - Suffixes used with aux verbs? 「そ」、「そう」のみ?
  * 名詞,非自立,一般,* 63 - Gerunding particles? 「こと」、「きらい」、「くせ」、「もの」.
  * 名詞,非自立,形容動詞語幹,* 64 - Gerunding particles that are verbs, i.e 「ふう」、「みたい」のみ.
  * 名詞,非自立,助動詞語幹,* 65 - Gerunding particles that are aux verbs i.e 「よ」、「よう」のみ.
  * 名詞,非自立,副詞可能,* 66 - Gerunding particles that are adverbs, i.e 「限り」、「さなか」、「うち」
  * 名詞,副詞可能,*,* 67 - Adverbial nouns, but seems mostly time descriptive stuff, i.e 「１０月」、「せんだって」、「当分」


### Script to extract the nouns out

    zcat data/tanaka-corpus.utf8.json.gz | pv -l --size 1760726 | python extract-nouns.py | gzip > nouns-fst.input.gz

### Testing FST on nouns

    zcat data/nouns-fst.input.gz | pv -l --size 40932 | flookup -xi nouns.fst | flookup -xi nouns-output.fst | grep -v '^$' | gzip > data/nouns-fst.output.gz

    zcat data/nouns-fst.input.gz | sed 's/ # sent_ids=.*//' > nouns.input
    zcat data/nouns-fst.output.gz > nouns.output
    paste nouns.input nouns.output > nouns-fst.side_by_side


### Notes

#### List of possible politeness prefix (`pos_id=30`)

    (u'お', 1904), (u'ご', 718), (u'大', 466), (u'今', 282), (u'新', 137), (u'御', 115), (u'全', 101), (u'老', 63), (u'不', 61), (u'無', 50), (u'小', 45), (u'再', 37), (u'同', 37), (u'両', 33), (u'各', 25), (u'超', 24), (u'非', 24), (u'最', 21), (u'未', 20), (u'総', 20), (u'ひと', 18), (u'微', 15), (u'高', 15), (u'本', 15), (u'ど', 14), (u'諸', 13), (u'元', 13), (u'前', 13), (u'低', 12), (u'猛', 12), (u'当', 12), (u'急', 10), (u'古', 10), (u'別', 9), (u'半', 8), (u'生', 8), (u'内', 7), (u'安', 7), (u'副', 7), (u'初', 7), (u'真っ', 7), (u'昨', 6), (u'反', 6), (u'好', 6), (u'ふた', 6), (u'子', 6), (u'旧', 6), (u'悪', 6), (u'女', 6), (u'駐', 5), (u'過', 5), (u'皆', 5), (u'名', 5), (u'後', 5), (u'まっ', 5), (u'真', 5), (u'す', 5), (u'表', 4), (u'逆', 4), (u'ド', 4), (u'多', 3), (u'近', 3), (u'来', 3), (u'直', 3), (u'み', 3), (u'現', 3), (u'純', 3), (u'焼', 3), (u'若', 3), (u'快', 3), (u'中', 3), (u'某', 3), (u'他', 3), (u'正', 3), (u'要', 2), (u'青', 2), (u'有', 2), (u'主', 2), (u'故', 2), (u'毎', 2), (u'平', 2), (u'ばか', 2), (u'もと', 2), (u'異', 2), (u'薄', 2), (u'少', 2), (u'分', 2), (u'先', 2), (u'被', 1), (u'常', 1), (u'代', 1), (u'原', 1), (u'省', 1), (u'負', 1), (u'親', 1), (u'貴', 1), (u'雄', 1), (u'片', 1), (u'打', 1), (u'実', 1), (u'入', 1), (u'可', 1), (u'即', 1), (u'次', 1), (u'長', 1), (u'偽', 1)

#### List of possible collective suffixes (`pos_id=51`)

    (u'たち', 5123), (u'達', 1837), (u'者', 1117), (u'人', 631), (u'家', 587), (u'語', 304), (u'会', 270), (u'性', 235), (u'書', 200), (u'力', 197), (u'日', 179), (u'員', 177), (u'品', 141), (u'学', 132), (u'通り', 119), (u'車', 115), (u'ら', 103), (u'物', 102), (u'手', 98), (u'間', 93), (u'局', 93), (u'部', 93), (u'側', 90), (u'どおり', 90), (u'ごと', 89), (u'用', 88), (u'地', 87), (u'機', 84), (u'国', 83), (u'目', 78), (u'屋', 77), (u'室', 75), (u'法', 71), (u'場', 71), (u'金', 70), (u'婦', 69), (u'店', 67), (u'心', 64), (u'ぶり', 63), (u'向き', 62), (u'園', 60), (u'賞', 57), (u'感', 57), (u'内', 56), (u'権', 56), (u'社', 54), (u'分', 53), (u'おき', 52), (u'式', 50), (u'所', 50), (u'先', 50), (u'券', 50), (u'量', 50), (u'率', 49), (u'費', 49), (u'士', 47), (u'つき', 47), (u'点', 47), (u'代', 44), (u'方', 43), (u'料', 42), (u'製', 42), (u'ども', 42), (u'だらけ', 40), (u'雨', 39), (u'っぱなし', 39), (u'医', 39), (u'界', 39), (u'外', 38), (u'便', 38), (u'げ', 38), (u'犯', 38), (u'よう', 38), (u'無し', 37), (u'線', 36), (u'号', 35), (u'街', 35), (u'師', 35), (u'官', 35), (u'隊', 34), (u'違い', 31), (u'罪', 30), (u'史', 30), (u'証', 30), (u'策', 30), (u'論', 30), (u'下', 30), (u'型', 29), (u'口', 29), (u'祭', 28), (u'軍', 27), (u'数', 27), (u'派', 26), (u'水', 26), (u'戦', 26), (u'札', 26), (u'事', 26), (u'向け', 26), (u'付き', 26), (u'高', 26), (u'形', 25), (u'類', 25), (u'画', 25), (u'長', 25), (u'すぎ', 24), (u'酒', 24), (u'係', 24), (u'面', 23), (u'状', 23), (u'体', 23), (u'表', 23), (u'団', 23), (u'帳', 22), (u'剤', 22), (u'館', 22), (u'生', 21), (u'案', 21), (u'しだい', 21), (u'誌', 20), (u'期', 20), (u'箱', 20), (u'病', 20), (u'知らず', 20), (u'かた', 19), (u'器', 19), (u'族', 19), (u'め', 18), (u'碑', 18), (u'台', 18), (u'沿い', 18), (u'紙', 17), (u'発', 17), (u'業', 17), (u'気味', 17), (u'風', 17), (u'建て', 16), (u'層', 16), (u'役', 16), (u'心地', 16), (u'済み', 15), (u'制', 15), (u'観', 15), (u'源', 15), (u'差', 14), (u'額', 14), (u'使い', 14), (u'服', 14), (u'道', 14), (u'食', 14), (u'もの', 14), (u'名', 14), (u'陣', 13), (u'計', 13), (u'っ子', 13), (u'主', 13), (u'職', 12), (u'別', 12), (u'あて', 12), (u'版', 12), (u'書き', 12), (u'作り', 12), (u'色', 11), (u'続き', 11), (u'等', 11), (u'順', 10), (u'あたり', 10), (u'子', 10), (u'署', 10), (u'放題', 10), (u'刈り', 10), (u'作', 10), (u'炎', 10), (u'民', 10), (u'じゅう', 10), (u'船', 10), (u'袋', 9), (u'談', 9), (u'校', 9), (u'ゆえ', 9), (u'馬', 9), (u'院', 9), (u'顔', 9), (u'玉', 9), (u'網', 8), (u'科', 8), (u'未満', 8), (u'食い', 8), (u'たて', 8), (u'取り', 8), (u'並み', 8), (u'付', 8), (u'痛', 8), (u'とも', 8), (u'症', 8), (u'薬', 8), (u'課', 7), (u'狂', 7), (u'切れ', 7), (u'級', 7), (u'図', 7), (u'乗り', 7), (u'犬', 7), (u'系', 7), (u'術', 7), (u'毎', 7), (u'付け', 7), (u'着', 7), (u'熱', 6), (u'星', 6), (u'引き', 6), (u'声', 6), (u'がけ', 6), (u'酔い', 6), (u'いかん', 6), (u'路', 6), (u'音', 6), (u'刑', 6), (u'半', 6), (u'曲', 6), (u'値', 6), (u'兵', 6), (u'材', 6), (u'立て', 6), (u'足らず', 5), (u'込み', 5), (u'年', 5), (u'難', 5), (u'帯', 5), (u'共', 5), (u'録', 5), (u'歌', 5), (u'愛', 5), (u'簿', 5), (u'王', 5), (u'届', 5), (u'大', 5), (u'安', 5), (u'坂', 5), (u'ごっこ', 5), (u'塗り', 5), (u'記', 5), (u'掛け', 5), (u'越し', 5), (u'像', 5), (u'癖', 5), (u'盛り', 5), (u'度', 5), (u'巡り', 5), (u'令', 4), (u'造り', 4), (u'児', 4), (u'圏', 4), (u'柄', 4), (u'張り', 4), (u'増', 4), (u'管', 4), (u'番', 4), (u'次', 4), (u'角', 4), (u'合わせ', 4), (u'ずくめ', 4), (u'なみ', 4), (u'光', 4), (u'嫌い', 4), (u'勝ち', 4), (u'比', 4), (u'買い', 4), (u'庫', 4), (u'離れ', 4), (u'骨', 4), (u'破り', 4), (u'ぎみ', 4), (u'いじり', 4), (u'づくり', 4), (u'ごこち', 3), (u'橋', 3), (u'艇', 3), (u'持ち', 3), (u'格', 3), (u'囚', 3), (u'待ち', 3), (u'湾', 3), (u'展', 3), (u'座', 3), (u'句', 3), (u'選', 3), (u'位', 3), (u'遣い', 3), (u'はだし', 3), (u'票', 3), (u'攻め', 3), (u'下り', 3), (u'味', 3), (u'ずみ', 3), (u'省', 3), (u'減', 3), (u'程度', 3), (u'灯', 3), (u'出', 3), (u'宛て', 3), (u'もと', 3), (u'まみれ', 3), (u'枠', 3), (u'とり', 3), (u'集', 3), (u'獣', 3), (u'あまり', 2), (u'岳', 2), (u'譲り', 2), (u'混じり', 2), (u'余り', 2), (u'卒', 2), (u'草', 2), (u'火', 2), (u'米', 2), (u'粉', 2), (u'損', 2), (u'油', 2), (u'相', 2), (u'揚げ', 2), (u'初', 2), (u'杯', 2), (u'板', 2), (u'差し', 2), (u'どうし', 2), (u'弾', 2), (u'鏡', 2), (u'城', 2), (u'漬け', 2), (u'堂', 2), (u'浴', 2), (u'じまい', 2), (u'げんか', 2), (u'筋', 2), (u'詰め', 2), (u'売', 2), (u'商', 2), (u'魚', 2), (u'神', 2), (u'給', 2), (u'流', 2), (u'艦', 2), (u'可', 2), (u'明け', 2), (u'ばり', 2), (u'支度', 2), (u'速', 2), (u'したて', 2), (u'訳', 2), (u'液', 2), (u'くず', 2), (u'細工', 2), (u'舎', 2), (u'沢山', 2), (u'打ち', 2), (u'入れ', 2), (u'づくし', 2), (u'枕', 1), (u'美', 1), (u'賃', 1), (u'落ち', 1), (u'球', 1), (u'樹', 1), (u'説', 1), (u'域', 1), (u'盤', 1), (u'糸', 1), (u'住まい', 1), (u'だこ', 1), (u'漁', 1), (u'ごころ', 1), (u'ぐらし', 1), (u'土', 1), (u'群', 1), (u'編', 1), (u'びな', 1), (u'がらみ', 1), (u'寄せ', 1), (u'返し', 1), (u'筆', 1), (u'ぐるみ', 1), (u'増し', 1), (u'痕', 1), (u'技', 1), (u'小', 1), (u'寄り', 1), (u'みち', 1), (u'楽', 1), (u'唄', 1), (u'塔', 1), (u'かげ', 1), (u'薄', 1), (u'皮', 1), (u'節', 1), (u'苦', 1), (u'質', 1), (u'具', 1), (u'疲れ', 1), (u'領', 1), (u'墳', 1), (u'不可', 1), (u'調', 1), (u'掘り', 1), (u'願', 1), (u'割り', 1), (u'池', 1), (u'伝', 1), (u'片', 1), (u'林', 1), (u'ぞろい', 1), (u'協', 1), (u'がわり', 1), (u'かげん', 1), (u'交じり', 1), (u'強', 1), (u'汁', 1), (u'限', 1), (u'マン', 1), (u'帰り', 1), (u'価', 1), (u'買', 1), (u'がかり', 1), (u'工', 1), (u'棟', 1), (u'さばき', 1), (u'電', 1), (u'弱', 1), (u'芸', 1), (u'漏れ', 1), (u'虫', 1), (u'止まり', 1), (u'通', 1), (u'種', 1), (u'出し', 1), (u'兄', 1), (u'回し', 1), (u'旗', 1), (u'ふう', 1), (u'替え', 1), (u'開き', 1), (u'波', 1)

#### List of possible honorific suffixes (`pos_id=55`)

    (u'さん', 770), (u'氏', 229), (u'君', 198), (u'様', 41), (u'ちゃん', 40), (u'さま', 25), (u'くん', 15), (u'どの', 7), (u'チャン', 4), (u'クン', 3), (u'ちん', 3), (u'著', 3), (u'サマ', 1)

#### List of possible pronouns (`pos_id=59`)

    (u'彼', 30282), (u'私', 26526), (u'彼女', 13334), (u'あなた', 4788), (u'君', 3993), (u'彼ら', 3601), (u'何', 2989), (u'それ', 2978), (u'誰', 1659), (u'これ', 1605), (u'我々', 1458), (u'ここ', 1458), (u'僕', 1325), (u'どこ', 1019), (u'そこ', 884), (u'みんな', 607), (u'いつ', 452), (u'どちら', 439), (u'これら', 406), (u'だれ', 381), (u'わたし', 331), (u'いくつ', 265), (u'どれ', 265), (u'われわれ', 255), (u'皆', 252), (u'ぼく', 194), (u'俺', 184), (u'あれ', 169), (u'それら', 162), (u'なん', 147), (u'こちら', 146), (u'みな', 129), (u'お前', 125), (u'あいつ', 122), (u'きみ', 121), (u'なに', 103), (u'彼等', 90), (u'やつ', 73), (u'あそこ', 72), (u'おまえ', 71), (u'君たち', 62), (u'貴方', 57), (u'あちこち', 56), (u'どっち', 51), (u'何処', 40), (u'僕ら', 37), (u'その他', 33), (u'いずれ', 33), (u'こっち', 32), (u'そちら', 30), (u'奴', 30), (u'オレ', 28), (u'みなさん', 26), (u'あちら', 23), (u'どなた', 20), (u'あたし', 19), (u'あんた', 19), (u'何時', 16), (u'己', 12), (u'ボク', 12), (u'僕達', 12), (u'彼女ら', 10), (u'あれこれ', 9), (u'そっ', 8), (u'おれ', 8), (u'われ', 8), (u'そっち', 8), (u'あっち', 7), (u'奴ら', 7), (u'奴等', 6), (u'みなさま', 6), (u'よそ', 5), (u'ここら', 4), (u'どっか', 4), (u'かなた', 4), (u'キミ', 2), (u'おのれ', 2), (u'我ら', 2), (u'わたくし', 2), (u'かしこ', 2), (u'わし', 2), (u'てめぇ', 1), (u'ウチ', 1), (u'吾輩', 1), (u'どこぞ', 1), (u'余', 1), (u'何れ', 1)

#### List of possible pronouns + collectives (`pos_id=59` + `pos_id=51`)

    (u'私たち', 3557), (u'私達', 1268), (u'僕たち', 61), (u'わたしたち', 54), (u'私ども', 41), (u'彼女たち', 24), (u'あなたたち', 22), (u'あなた方', 19), (u'君達', 13), (u'ぼくたち', 12), (u'あれら', 11), (u'何物', 11), (u'貴方達', 9), (u'何語', 8), (u'君たち', 8), (u'やつら', 7), (u'俺たち', 7), (u'それゆえ', 6), (u'あいつら', 5), (u'こちら側', 5), (u'あなた達', 4), (u'私あて', 4), (u'みんな家', 4), (u'わたしら', 4), (u'いつ家', 4), (u'君ら', 3), (u'きみたち', 3), (u'俺達', 3), (u'あなたしだい', 2), (u'皆手', 2), (u'お前ら', 2), (u'僕等', 2), (u'あなたあて', 2), (u'何杯', 2), (u'おまえら', 2), (u'わたくしたち', 2), (u'君無し', 2), (u'どちら側', 2), (u'みんな手', 2), (u'彼女達', 2), (u'わたし達', 2), (u'ぼくら', 2), (u'君宛て', 2), (u'きみら', 1), (u'いつ雨', 1), (u'おまえ達', 1), (u'やつあたり', 1), (u'いくつめ', 1), (u'何号', 1), (u'おまえたち', 1), (u'おれたち', 1), (u'彼無し', 1), (u'私共', 1), (u'みな水', 1), (u'みんな神', 1), (u'みな船', 1), (u'みんな嫌い', 1), (u'いつ側', 1), (u'みんな口', 1), (u'なに語', 1), (u'我々ら', 1), (u'われら', 1), (u'皆国', 1), (u'ボクら', 1), (u'そこ知らず', 1), (u'皆会', 1), (u'彼女顔', 1), (u'お前たち', 1), (u'何座', 1), (u'みんな順', 1), (u'あんたら', 1), (u'彼ら側', 1), (u'てめぇら', 1), (u'彼しだい', 1), (u'君あて', 1), (u'俺ら', 1), (u'こっち生', 1), (u'みな金', 1), (u'彼目', 1), (u'あいつめ', 1), (u'何名', 1), (u'何分', 1), (u'あなた宛て', 1), (u'みんな金', 1), (u'ここ数', 1), (u'何色', 1), (u'君しだい', 1), (u'僕高', 1), (u'なにげ', 1), (u'何所', 1)

#### List of gerunding particles (`pos_id=63`)

    (u'こと', 10954), (u'の', 10930), (u'もの', 2708), (u'ん', 1551), (u'事', 1390), (u'方', 855), (u'つもり', 799), (u'ほう', 586), (u'わけ', 402), (u'はず', 301), (u'点', 270), (u'物', 241), (u'気', 238), (u'者', 210), (u'せい', 152), (u'ふり', 78), (u'もん', 46), (u'もと', 46), (u'日', 27), (u'くせ', 22), (u'どころ', 22), (u'訳', 18), (u'かた', 14), (u'ン', 11), (u'程', 8), (u'とき', 6), (u'へん', 6), (u'筈', 5), (u'すべ', 4), (u'かい', 4), (u'旨', 4), (u'はずみ', 3), (u'こなし', 3), (u'割', 3), (u'すえ', 1), (u'コト', 1), (u'折', 1), (u'たぐい', 1), (u'がい', 1), (u'下', 1), (u'かけ', 1), (u'種', 1), (u'ため', 1), (u'さま', 1), (u'ふし', 1), (u'きらい', 1), (u'ホ', 1), (u'機', 1), (u'生み', 1), (u'モノ', 1)

#### List of pronouns + no (`pos_id=59` + `pos_id=63`)

    (u'私の', 65), (u'あなたの', 30), (u'君の', 21), (u'彼の', 13), (u'彼女の', 11), (u'誰の', 9), (u'僕の', 8), (u'なにもの', 3), (u'そこの', 2), (u'我々の', 2), (u'ぼくの', 2), (u'だれの', 2), (u'わたしの', 2), (u'よその', 1), (u'きみの', 1), (u'みんな気', 1), (u'それどころ', 1)

## Preparing verbs

### MeCab verb classification

    助詞,接続助詞,*,* 18
    助動詞,*,*,* 25
    動詞,自立,*,* 31
    動詞,接尾,*,* 32
    動詞,非自立,*,* 33

### List of verb-related particles (`pos_id=18`)

    (u'て', 48591), (u'ば', 4989), (u'で', 3732), (u'が', 3175), (u'と', 2535), (u'ので', 1937), (u'から', 1279), (u'のに', 725), (u'ながら', 365), (u'けど', 331), (u'し', 234), (u'つつ', 105), (u'ちゃ', 93), (u'とも', 92), (u'けれども', 91), (u'けれど', 88), (u'やいなや', 47), (u'ど', 37), (u'どころか', 25), (u'からには', 19), (u'や', 12), (u'なり', 12), (u'ものの', 12), (u'たって', 11), (u'じゃ', 9), (u'んで', 9), (u'ども', 8), (u'けども', 3), (u'ちゃあ', 1), (u'および', 1)

### List of auxiliary verbs (`pos_id=25`)

    (u'た', 61300), (u'ない', 12421), (u'ます', 10756), (u'う', 6644), (u'なかっ', 3973), (u'まし', 3818), (u'ん', 3386), (u'ませ', 3304), (u'だ', 3016), (u'なけれ', 2065), (u'だろ', 2064), (u'たい', 1896), (u'たら', 1541), (u'べき', 1391), (u'でしょ', 966), (u'ず', 889), (u'なく', 815), (u'ましょ', 792), (u'で', 412), (u'です', 378), (u'たく', 352), (u'でし', 311), (u'つ', 297), (u'ね', 278), (u'なら', 247), (u'ぬ', 170), (u'ざる', 154), (u'だっ', 128), (u'たかっ', 127), (u'ある', 123), (u'らしい', 113), (u'あろ', 88), (u'まい', 78), (u'だら', 54), (u'なきゃ', 36), (u'な', 28), (u'たけれ', 24), (u'たろ', 19), (u'き', 18), (u'あっ', 14), (u'やし', 11), (u'べから', 11), (u'べし', 9), (u'じ', 6), (u'らし', 5), (u'たる', 5), (u'し', 4), (u'る', 4), (u'べく', 4), (u'り', 3), (u'ずん', 3), (u'らしく', 3), (u'ざれ', 2), (u'なけりゃ', 2), (u'やん', 2), (u'なくっ', 2), (u'ねえ', 2), (u'あり', 2), (u'無い', 2), (u'らしかっ', 2), (u'たし', 1), (u'なかろ', 1), (u'ねぇ', 1), (u'無', 1), (u'やしょ', 1), (u'たり', 1), (u'なき', 1)

    # dictionary forms
    (u'た', 62668), (u'ない', 19327), (u'ます', 18670), (u'う', 6644), (u'だ', 5941), (u'ん', 3386), (u'たい', 2592), (u'です', 1655), (u'ぬ', 1496), (u'べし', 1415), (u'つ', 297), (u'ある', 227), (u'らしい', 123), (u'まい', 78), (u'き', 22), (u'やす', 12), (u'り', 6), (u'たり', 6), (u'じ', 6), (u'無い', 3), (u'やん', 2), (u'る', 1)


### List of verb suffixes (`pos_id=32`)

    (u'れ', 4606), (u'られ', 1806), (u'せ', 1224), (u'れる', 1169), (u'られる', 455), (u'せる', 339), (u'がっ', 128), (u'させ', 100), (u'せよ', 68), (u'がる', 45), (u'させる', 34), (u'がら', 26), (u'させよ', 16), (u'れれ', 11), (u'し', 10), (u'がり', 7), (u'しゃ', 6), (u'れよ', 5), (u'せれ', 5), (u'す', 4), (u'られれ', 4), (u'さ', 3), (u'せろ', 3), (u'れろ', 2), (u'れりゃ', 1), (u'れん', 1), (u'しめ', 1), (u'られよ', 1), (u'さし', 1)

    # dictionary forms
    (u'れる', 5795), (u'られる', 2266), (u'せる', 1639), (u'がる', 206), (u'させる', 150), (u'す', 23), (u'さす', 1), (u'しめる', 1)


### List of dependent verbs (`pos_id=33`) (dictionary forms)

    (u'いる', 24514), (u'くれる', 2343), (u'なさる', 2317), (u'くる', 2051), (u'なる', 2034), (u'くださる', 1751), (u'しまう', 1468), (u'下さる', 1048), (u'いく', 740), (u'おく', 703), (u'てる', 693), (u'みる', 623), (u'もらう', 594), (u'行く', 406), (u'始める', 361), (u'いける', 344), (u'やる', 302), (u'あげる', 270), (u'すぎる', 240), (u'続ける', 229), (u'来る', 201), (u'もらえる', 182), (u'ある', 176), (u'おる', 136), (u'ちゃう', 134), (u'出す', 129), (u'いただく', 117), (u'終える', 111), (u'過ぎる', 111), (u'はじめる', 84), (u'だす', 82), (u'かける', 77), (u'できる', 70), (u'合う', 66), (u'かかる', 66), (u'つづける', 63), (u'きれる', 46), (u'かねる', 44), (u'きる', 39), (u'く', 39), (u'回る', 35), (u'得る', 33), (u'終わる', 30), (u'とく', 28), (u'うる', 27), (u'あう', 25), (u'まわる', 24), (u'ゆく', 22), (u'直す', 20), (u'でる', 20), (u'いらっしゃる', 19), (u'上げる', 19), (u'つける', 17), (u'切れる', 15), (u'切る', 14), (u'とる', 14), (u'える', 11), (u'いたす', 11), (u'こむ', 11), (u'まいる', 10), (u'みせる', 10), (u'尽くす', 10), (u'込む', 9), (u'頂く', 8), (u'らっしゃる', 7), (u'まくる', 7), (u'のける', 7), (u'ちまう', 7), (u'抜く', 5), (u'なおす', 4), (u'じゃう', 4), (u'致す', 3), (u'ぬく', 3), (u'そこねる', 3), (u'つくす', 3), (u'続く', 2), (u'どる', 2), (u'どく', 2), (u'づける', 2), (u'たげる', 2), (u'はる', 1), (u'そびれる', 1), (u'願える', 1), (u'願う', 1), (u'参る', 1)


### Build the verb lexicon that we can use

    zcat data/tanaka-corpus.utf8.json.gz | pv -l --size 1760726 | python scripts/extract-lexicon.py --tanaka-corpus > lexicon/tanaka-corpus.lexicon.json
    python scripts/extract-lexicon.py --jmdict-corpus < corpora/JMdict.gz > lexicon/jmdict-corpus.lexicon.json
    python scripts/build-lexicon.py lexicon/tanaka-corpus.lexicon.json lexicon/jmdict-corpus.lexicon.json

    zcat data/tanaka-corpus.utf8.json.gz | pv -l --size 1760726 | python scripts/extract-terms.py --verbs --details > verb-types.details.txt
    zcat data/tanaka-corpus.utf8.json.gz | pv -l --size 1760726 | python scripts/extract-terms.py --verbs | gzip > verb-types.input.gz
    zcat data/tanaka-corpus.utf8.json.gz | pv -l --size 1760726 | python scripts/extract-terms.py --verbs --all | gzip > verb-all.input.gz

    zcat data/tanaka-corpus.utf8.json.gz | pv -l --size 1760726 | python scripts/extract-terms.py --adjectives --details > adj-types.details.txt
    zcat data/tanaka-corpus.utf8.json.gz | pv -l --size 1760726 | python scripts/extract-terms.py --adjectives | gzip > adj-types.input.gz
    zcat data/tanaka-corpus.utf8.json.gz | pv -l --size 1760726 | python scripts/extract-terms.py --adjectives --all | gzip > adj-all.input.gz

    zcat verb-all.input.gz | shuffle | head -n 2000 | sort | gzip > verb-eval_2k.input.gz
    zcat adj-all.input.gz | shuffle | head -n 1000 | sort | gzip > adj-eval_1k.input.gz

    zcat evaluation/verb-eval_2k.input.gz | sort | uniq | wc -l # 1105 unique items
    zcat evaluation/adj-eval_1k.input.gz | sort | uniq | wc -l # 476 unique items

    zcat evaluation/verb-eval_2k.input.gz | flookup verbs.fst > verb-eval_2k.output
    zcat evaluation/adj-eval_1k.input.gz | flookup verbs.fst > adj-eval_1k.output
